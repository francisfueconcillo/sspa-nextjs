## SSPA with NextJS
Integration of SSPA with Next JS


* Currently, it only runs sspa by webpack-cli (outside nextjs) so that it can be hosted in Vercel
* Original package.json commands:
```
"scripts": {
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "lint": "next lint",
    "sspa": "webpack serve --port 9000 --env isLocal"
  },

```

### Status
URL: https://sspa-nextjs.vercel.app/

- The `/api` and NextJs pages doesnt workbecause package.json's  `npm run  start` is using webpack-cli to run the app instead of `next start`
- Loading of external JS should be possible